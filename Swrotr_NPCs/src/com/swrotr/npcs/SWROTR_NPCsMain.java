package com.swrotr.npcs;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Equipment;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;



/**
 * Created by storm on 8/18/2017.
 */
public class SWROTR_NPCsMain extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPreCommand(PlayerCommandPreprocessEvent e) {
        String msg = e.getMessage();
        if(msg.startsWith("/"))
        if(msg.equalsIgnoreCase("npc arm") || msg.equalsIgnoreCase("npc hand") || msg.equalsIgnoreCase("npc head") || msg.equalsIgnoreCase("npc hat")) {
            ItemStack hand = e.getPlayer().getInventory().getItemInMainHand();
            if(hand == null) {
                e.getPlayer().sendMessage(ChatColor.RED + "\n\nYou must have an item in your main hand in order to use this command!");
                return;
            }

            NPC npc = CitizensAPI.getDefaultNPCSelector().getSelected(e.getPlayer());
            if(npc == null) {
                e.getPlayer().sendMessage(ChatColor.RED + "\n\nYou must have an NPC selected in order to use this command!");
                return;
            }

            Equipment eq = npc.getTrait(Equipment.class);
            if(msg.equalsIgnoreCase("npc arm") || msg.equalsIgnoreCase("npc hand"))
                eq.set(Equipment.EquipmentSlot.HAND, hand);
            else
                eq.set(Equipment.EquipmentSlot.HELMET, hand);
            npc.addTrait(eq);
        }

    }

}
