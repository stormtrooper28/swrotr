package com.swrotr.greetings;

/**
 * Created by stormtrooper28 on 11/2/2016.
 */

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.CachedServerIcon;

import java.io.File;
import java.util.*;

public class SWROTR_GreetingsMain extends JavaPlugin implements Listener {
    // 5d193b2ae64c
    private static final String[] MOTDs = { "Star Wars Return of the Republic", "Star Wars: Return of the Republic"};
    
    List<CachedServerIcon> icons = new ArrayList<>();

    @Override
    public void onEnable() {
        String root = new File(".").getAbsolutePath();
        String slash = "\\";
        if (root.contains("/"))
            slash = "/";

        File iconFolder = new File(root + slash + "ServerIcons");

        for (File img : iconFolder.listFiles())
            try {
                icons.add(Bukkit.loadServerIcon(img));
            } catch (Exception e) {

            }

        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void setServerGreetings(ServerListPingEvent e) {
        Random r = new Random();

        e.setMotd(MOTDs[r.nextInt(MOTDs.length)]);

        e.setServerIcon(icons.get(r.nextInt(icons.size())));

    }
}
