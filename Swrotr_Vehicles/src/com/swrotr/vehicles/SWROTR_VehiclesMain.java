package com.swrotr.vehicles;

import com.swrotr.vehicles.Vehicles.Hover.Hover;
import com.swrotr.vehicles.Vehicles.Hover.HoverBike;
import com.swrotr.vehicles.Vehicles.Vehicle;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.UUID;

import static java.lang.Math.sin;

/**
 * Created by storm on 8/18/2017.
 */
public class SWROTR_VehiclesMain extends JavaPlugin implements Listener {
    public static SWROTR_VehiclesMain plugin;

    public static Field f = null;

    @Override
    public void onEnable() {
        try {
            f = EntityLiving.class.getDeclaredField("bd");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            Bukkit.shutdown();
        }

        plugin = this;

        if(getConfig().contains("vehicles.hovers.hoverbikes")) {
            ConfigurationSection k = getConfig().getConfigurationSection("vehicles.hovers.hoverbikes");
            for(String key : k.getKeys(false)) {
                new HoverBike(new Location(Bukkit.getWorld(UUID.fromString(k.getConfigurationSection(key).getString("world"))),
                        k.getConfigurationSection(key).getDouble("x"),
                        k.getConfigurationSection(key).getDouble("y"),
                        k.getConfigurationSection(key).getDouble("z"),
                        (float) k.getConfigurationSection(key).getDouble("yaw"),
                        (float) k.getConfigurationSection(key).getDouble("pitch")
                ));
            }
        }

        getConfig().set("vehicles", null);

        Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
            double t = 0;
            @Override
            public void run() {
                for(ArmorStand as : Vehicle.vehicles.keySet()) {
                    if(Vehicle.vehicles.keySet().contains(as) && Vehicle.vehicles.get(as) instanceof Hover) {
                        if (as.getPassengers().isEmpty()) {
                            Location l = as.getLocation();
                            as.setVelocity(new Vector(as.getVelocity().getX(), 0, as.getVelocity().getZ()));
                            as.teleport(new Location(as.getWorld(), l.getX(), l.getY() - .01 * sin(t), l.getZ()));
                        } else {
                            Player p = (Player) as.getPassengers().get(0);
                            Location l = as.getLocation();
                            as.setVelocity(new Vector(as.getVelocity().getX(), 0 - .01 * sin(t), as.getVelocity().getZ()));
                        }
                        t += Math.PI * .01;
                        if (t == (Math.PI * 2)) t = 0;
                    }
                }
            }
        }, 0, 1);

        Bukkit.getScheduler().runTaskTimer(this, () -> {
            for(ArmorStand as : Vehicle.vehicles.keySet()) {
                Vehicle.MovementType mt = Vehicle.MovementType.STILL;
                Vehicle.RotateType rt = Vehicle.RotateType.STRAIGHT;
                boolean isBoosted = false;

                if(!as.getPassengers().isEmpty() && as.getPassengers().get(0) instanceof Player) {
                    EntityPlayer ep = ((CraftPlayer) as.getPassengers().get(0)).getHandle();

                    if(ep.be > 0)               rt = Vehicle.RotateType.LEFT;
                    else if (ep.be < 0)         rt = Vehicle.RotateType.RIGHT;

                    if(ep.bg > 0)               mt = Vehicle.MovementType.ACCELERATE;
                    else if (ep.bg < 0)         mt = Vehicle.MovementType.DECELERATE;

                    try {
                        if(f.getBoolean(ep))    isBoosted = true;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                else
                    mt = Vehicle.MovementType.DECELERATE;

                Vehicle.vehicles.get(as).move(mt, rt, isBoosted);
            }
        },0,5);

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        int i = 0;

        getConfig().createSection("vehicles.hovers.hoverbikes");
        ConfigurationSection key = getConfig().getConfigurationSection("vehicles.hovers.hoverbikes");

        for(ArmorStand as : Vehicle.vehicles.keySet()) {
            key.createSection(String.valueOf(i));
            ConfigurationSection k = key.getConfigurationSection(String.valueOf(i));


            Location l = as.getLocation();
            k.set("world", as.getWorld().getUID().toString());
            k.set("x", l.getX());
            k.set("y", l.getY());
            k.set("z", l.getZ());

            //k.set("pitch", l.getPitch());
            k.set("yaw", as.getHeadPose().getY());

            k.set("head", as.getHelmet().getType().toString());
            k.set("type", Vehicle.VehicleType.HOVER_BIKE.toString());
            k.set("model", HoverBike.Model.DEFAULT.toString());

            as.setHelmet(new ItemStack(Material.AIR));

            ++i;
        }
        saveConfig();

        Vehicle.vehicles.keySet().forEach(Entity::remove);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = (Player) sender;

        new HoverBike(p.getLocation());

        return true;
    }

    @EventHandler
    public void onArmorStandEquip(PlayerArmorStandManipulateEvent e) {
        if(e.getRightClicked() instanceof ArmorStand){
            ArmorStand as = (ArmorStand) e.getRightClicked();
            if(!Vehicle.vehicles.keySet().isEmpty() && Vehicle.vehicles.keySet().contains(as)) {
               e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onClickOnVehicle(PlayerInteractAtEntityEvent e) {
        if(e.getRightClicked() instanceof ArmorStand){
            ArmorStand as = (ArmorStand) e.getRightClicked();
            if(!Vehicle.vehicles.keySet().isEmpty() && Vehicle.vehicles.keySet().contains(as)) {
                e.setCancelled(true);

                as.addPassenger(e.getPlayer());
            }
        }
    }

    @EventHandler
    public void onArmorStandDeath(EntityDeathEvent e) {
        if(e.getEntity() instanceof ArmorStand && Vehicle.vehicles.keySet().contains((ArmorStand) e.getEntity())) {
            Vehicle.vehicles.remove((ArmorStand) e.getEntity());
        }
    }

}


/*

            try {
                // 1.12
                try {
                    if (!current) {
                        throw new Exception("Not 1.12");
                    }
                    class_Entity_jumpingField = class_EntityLiving.getDeclaredField("bd");
                    class_Entity_jumpingField.setAccessible(true);
                    class_Entity_moveStrafingField = class_EntityLiving.getDeclaredField("be");
                    class_Entity_moveForwardField = class_EntityLiving.getDeclaredField("bg");
                } catch (Throwable not12) {
                    // 1.11
                    current = false;
                    try {
                        class_Entity_jumpingField = class_EntityLiving.getDeclaredField("bd");
                        class_Entity_jumpingField.setAccessible(true);
                        class_Entity_moveStrafingField = class_EntityLiving.getDeclaredField("be");
                        class_Entity_moveForwardField = class_EntityLiving.getDeclaredField("bf");
                    } catch (Throwable not11) {
                        // 1.10
                        try {
                            class_Entity_jumpingField = class_EntityLiving.getDeclaredField("be");
                            class_Entity_jumpingField.setAccessible(true);
                            class_Entity_moveStrafingField = class_EntityLiving.getDeclaredField("bf");
                            class_Entity_moveForwardField = class_EntityLiving.getDeclaredField("bg");
                        } catch (Throwable not10) {
                            class_Entity_jumpingField = class_EntityLiving.getDeclaredField("bc");
                            class_Entity_jumpingField.setAccessible(true);
                            class_Entity_moveStrafingField = class_EntityLiving.getDeclaredField("bd");
                            class_Entity_moveForwardField = class_EntityLiving.getDeclaredField("be");
                        }
                    }
                }
            } catch (Throwable ex) {
                Bukkit.getLogger().log(Level.WARNING, "An error occurred while registering entity movement accessors, vehicle control will not work", ex);
                class_Entity_jumpingField = null;
                class_Entity_moveStrafingField = null;
                class_Entity_moveForwardField = null;
            }
 */