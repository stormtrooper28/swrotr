package com.swrotr.vehicles.Vehicles.Hover;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

/**
 * Created by storm on 8/19/2017.
 */
public class HoverBike extends Hover{
    public enum Model {
        DEFAULT(2);

        private short damageValue;
        private Model(int damageValue) {
            this.damageValue = (short) damageValue;
        }

        public short getDamageValue() { return damageValue; }
    }

    private Vector velocity;
    private Model model;
    private static final double MAX_SPEED = 3, ACCELERATION = 0.5, DECELERATION = 0.25, ROTATION_FACTOR = Math.PI / 16;
    private double speed = 0;

    public HoverBike(Location l) {
        this(l, Model.DEFAULT);
    }

    public HoverBike(Location l, Model model) {
        super(l);

        this.type = VehicleType.HOVER_BIKE;
        this.model = model;

        ItemStack hoverBikeItem = new ItemStack(vehicleMat);
        hoverBikeItem.setDurability(model.getDamageValue());
        as.setHelmet(hoverBikeItem);
    }

    @Override
    public boolean move(MovementType mt, RotateType rt, boolean isBoosted) {
        boolean moveDown = (as.getLocation().getBlock().getRelative(0, -1, 0).getType() == Material.AIR) ? true : false;
        boolean moveUp;

        Vector speedVector, heightVector, directionVector = eulerToVector(as.getHeadPose());

        switch(mt) {
            case STILL:
                // Speed of the hover bike
                speedVector = new Vector(directionVector.getX(), 1, directionVector.getZ()).normalize().multiply(((isBoosted) ? 2 : 1) *speed);
                speedVector.setY(0);

                // Leave the hover bike hovering (does not account for changing terrain)
                heightVector = new Vector(0, (moveDown) ? -1 : as.getVelocity().getY(), 0);

                // Move forward, maintain height-hover
                as.setVelocity(speedVector.add(heightVector));
                break;
            case ACCELERATE:
                speed = (speed <= (MAX_SPEED - ACCELERATION)) ? speed + ACCELERATION : MAX_SPEED;

                // Speed & direction of the hover bike
                speedVector = new Vector(directionVector.getX(), 1, directionVector.getZ()).normalize().multiply(((isBoosted) ? 2 : 1) * speed);
                speedVector.setY(0);

                // Leave the hover bike hovering (does not account for changing terrain)
                heightVector = new Vector(0, (moveDown) ? -1 : as.getVelocity().getY(), 0);


                // Move forward, maintain height-hover
                as.setVelocity(speedVector.add(heightVector));
                break;
            case DECELERATE:
                speed = (speed >= (0 + DECELERATION)) ? speed - DECELERATION : 0;
                // Speed of the hover bike
                speedVector = new Vector(directionVector.getX(), 1, directionVector.getZ()).normalize().multiply(speed);
                speedVector.setY(0);

                // Leave the hover bike hovering (does not account for changing terrain)
                heightVector = new Vector(0, (moveDown) ? -1 : as.getVelocity().getY(), 0);

                // Move forward, maintain height-hover
                as.setVelocity(speedVector.add(heightVector));
                break;
        }

        switch(rt) {
            case LEFT:
                as.setHeadPose(new EulerAngle(0, as.getHeadPose().getY() - ROTATION_FACTOR / ((isBoosted) ? 2 : 1), 0));
                break;
            case RIGHT:
                as.setHeadPose(new EulerAngle(0, as.getHeadPose().getY() + ROTATION_FACTOR / ((isBoosted) ? 2 : 1), 0));
                break;
            case STRAIGHT:
                break;
        }
        return true;
    }
    private Vector eulerToVector(EulerAngle ea) {
        double theta = ea.getY(), x, z;
        x = Math.cos(theta);
        z = Math.sin(theta);

        return new Vector(x, 0, z);
    }
}
