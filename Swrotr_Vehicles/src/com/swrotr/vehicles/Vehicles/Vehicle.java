package com.swrotr.vehicles.Vehicles;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.util.EulerAngle;

import java.util.HashMap;

/**
 * Created by storm on 8/19/2017.
 */
abstract public class Vehicle {
    public enum VehicleType {
        HOVER_BIKE;
    }

    public enum MovementType {
        ACCELERATE, DECELERATE, STILL;
    }

    public enum RotateType {
       LEFT, RIGHT, STRAIGHT;
    }

    public static HashMap<ArmorStand, Vehicle> vehicles = new HashMap<>();
    protected static Material vehicleMat = Material.JACK_O_LANTERN;

    protected ArmorStand as;
    protected VehicleType type;

    protected Vehicle(Location l) {
        l.getWorld().loadChunk(l.getChunk());
        as = (ArmorStand) l.getWorld().spawnEntity(l, EntityType.ARMOR_STAND);
        as.setHeadPose(new EulerAngle(0, ((l.getYaw() / 90) * (Math.PI)), 0));

        as.setBasePlate(false);
        as.setVisible(false);
        as.setInvulnerable(true);
        as.setCollidable(false);
        as.setSmall(true);

        vehicles.put(as, this);
    }

    abstract public boolean move(MovementType mt, RotateType rt, boolean isBoosted);
}
